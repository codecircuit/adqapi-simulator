#pragma once

#include <cstdint>

struct ADQRecordHeader {
	uint8_t Channel;
	uint8_t DataFormat;
	uint16_t GeneralPurpose0;
	uint16_t GeneralPurpose1;
	uint32_t RecordLength;
	uint32_t RecordNumber;
	int64_t RecordStart;
	uint8_t RecordStatus;
	int32_t SamplePeriod;
	uint32_t SerialNumber;
	uint64_t Timestamp;
	uint8_t UserID;
};

void* CreateADQControlUnit();

int ADQControlUnit_FindDevices(void* adq_cu_ptr);

unsigned int ADQControlUnit_EnableErrorTrace(void* adq_cu_ptr,
                                             unsigned int trace_level,
                                             const char* trace_file_dir);
void DeleteADQControlUnit(void* adq_cu_ptr);
int ADQControlUnit_GetFailedDeviceCount(void* adq_cu_ptr);
int ADQ_HasFeature(void* adq_cu_ptr, int adq_no, const char* feature);
int ADQ_StopStreaming(void* adq_cu_ptr, int adq_no);
int ADQ_StartStreaming(void* adq_cu_ptr, int adq_no);
int ADQ_ArmTrigger(void* adq_cu_ptr, int adq_no);
int ADQ_DisarmTrigger(void* adq_cu_ptr, int adq_no);
int ADQ_SetMixerFrequency(void* adq_cu_ptr, int adq_no, int, double);
int ADQ_SetMixerPhase(void* adq_cu_ptr, int adq_no, int, int);
int ADQ_SetChannelDecimation(void* adq_cu_ptr, int adq_no, int, int);
int ADQ_SetDataFormat(void* adq_cu_ptr, int adq_no, int);
int ADQ_ForceResynchronizationSDR(void* adq_cu_ptr, int adq_no);
int ADQ_SetTriggerMode(void* adq_cu_ptr, int adq_no, int);
int ADQ_SetInternalTriggerPeriod(void* adq_cu_ptr, int adq_no, int);
int ADQ_SetupTriggerOutput(void* adq_cu_ptr, int adq_no, int, int, int, int);
int ADQ_SetExternTrigEdge(void* adq_cu_ptr, int adq_no, int);
int ADQ_SetTriggerInputImpedance(void* adq_cu, int adq_no, int, int);
int ADQ_TriggeredStreamingSetup(void* adq_cu_ptr,
                                int adq_no,
                                unsigned,
                                int,
                                int,
                                int,
                                unsigned);
int ADQ_SetStreamStatus(void* adq_cu_ptr, int adq_no, int);
int ADQ_SetTestPatternMode(void* adq_cu_ptr, int adq_no, int);
int ADQ_SetClockSource(void* adq_cu_ptr, int adq_no, int);

int ADQ_SetTransferBuffers(void* adq_cu_ptr,
                           int adq_no,
                           unsigned int num_transfer_buffers,
                           unsigned int transfer_buffer_size);
int ADQ_GetTransferBufferStatus(void* adq_cu_ptr, int, unsigned int* filled);
int ADQ_FlushDMA(void* adq_cu_ptr, int);
int ADQ_GetDataStreaming(void* adq_cu_ptr,
                         int adq_no,
                         void** target_buffers,
                         void** target_headers,
                         unsigned char channel_mask,
                         unsigned int* samples_added,
                         unsigned int* headers_added,
                         unsigned int* header_status);
