#include <adqapi-simulator/ADQAPI.h>

#include <exception>
#include <iostream>
#include <random>

void* CreateADQControlUnit() {
	return nullptr;
}

int ADQControlUnit_FindDevices(void* adq_cu_ptr) {
	return 1;
}

unsigned int ADQControlUnit_EnableErrorTrace(void* adq_cu_ptr,
                                             unsigned int trace_level,
                                             const char* trace_file_dir) {
	return 1;
}

void DeleteADQControlUnit(void* adq_cu_ptr) {
}

int ADQControlUnit_GetFailedDeviceCount(void* adq_cu_ptr) {
	return 0;
}

static unsigned gl_buffers_filled;

int ADQ_HasFeature(void* adq_cu_ptr, int adq_no, const char* feature) {
	return 1;
}

int ADQ_StopStreaming(void* adq_cu_ptr, int adq_no) {
	return 1;
}
int ADQ_StartStreaming(void* adq_cu_ptr, int adq_no) {
	gl_buffers_filled = 1;
	return 1;
}

static unsigned gl_num_transfer_buffers;
static unsigned gl_transfer_buffer_size;

int ADQ_SetTransferBuffers(void* adq_cu_ptr,
                           int adq_no,
                           unsigned int num_transfer_buffers,
                           unsigned int transfer_buffer_size) {
	gl_num_transfer_buffers = num_transfer_buffers;
	gl_transfer_buffer_size = transfer_buffer_size;
	return 1;
}

int ADQ_GetTransferBufferStatus(void* adq_cu_ptr, int, unsigned int* filled) {
	*filled = gl_buffers_filled;
	return 1;
}

int ADQ_FlushDMA(void* adq_cu_ptr, int) {
	return 1;
}
int ADQ_GetDataStreaming(void* adq_cu_ptr,
                         int adq_no,
                         void** target_buffers,
                         void** target_headers,
                         unsigned char channel_mask,
                         unsigned int* samples_added,
                         unsigned int* headers_added,
                         unsigned int* header_status) {

	std::default_random_engine rng;
	std::uniform_int_distribution<int16_t> dist(0, 128);
	std::uniform_int_distribution<int> dist_num_samples(1024, 2048);
	if (channel_mask != 0b11111111) {
		std::cerr << "ERROR: channel_mask != 0b11111111 not supported"
		          << std::endl;
		std::terminate();
	}

	constexpr int num_data_channels = 8;
	for (int ci = 0; ci < num_data_channels; ++ci) {

		//		const int num_samples = dist_num_samples(rng); // enable for varying number of samples for each channel
		const int num_samples = 769;
		int16_t* curr_buffer = reinterpret_cast<int16_t*>(target_buffers[ci]);
		for (int i = 0; i < num_samples; ++i) {
			curr_buffer[i] = dist(rng);
		}
		samples_added[ci] = num_samples;
	}

	return 1;
}

//
// Empty functions
//
int ADQ_ArmTrigger(void* adq_cu_ptr, int adq_no) {
	return 1;
}
int ADQ_DisarmTrigger(void* adq_cu_ptr, int adq_no) {
	return 1;
}
int ADQ_SetMixerFrequency(void* adq_cu_ptr, int adq_no, int, double) {
	return 1;
}
int ADQ_SetMixerPhase(void* adq_cu_ptr, int adq_no, int, int) {
	return 1;
}
int ADQ_SetChannelDecimation(void* adq_cu_ptr, int adq_no, int, int) {
	return 1;
}
int ADQ_SetDataFormat(void* adq_cu_ptr, int adq_no, int) {
	return 1;
}
int ADQ_ForceResynchronizationSDR(void* adq_cu_ptr, int adq_no) {
	return 1;
}
int ADQ_SetTriggerMode(void* adq_cu_ptr, int adq_no, int) {
	return 1;
}
int ADQ_SetInternalTriggerPeriod(void* adq_cu_ptr, int adq_no, int) {
	return 1;
}
int ADQ_SetupTriggerOutput(void* adq_cu_ptr, int adq_no, int, int, int, int) {
	return 1;
}
int ADQ_SetExternTrigEdge(void* adq_cu_ptr, int adq_no, int) {
	return 1;
}
int ADQ_SetTriggerInputImpedance(void* adq_cu, int adq_no, int, int) {
	return 1;
}

int ADQ_TriggeredStreamingSetup(void* adq_cu_ptr,
                                int adq_no,
                                unsigned,
                                int,
                                int,
                                int,
                                unsigned) {
	return 1;
}
int ADQ_SetStreamStatus(void* adq_cu_ptr, int adq_no, int) {
	return 1;
}
int ADQ_SetTestPatternMode(void* adq_cu_ptr, int adq_no, int) {
	return 1;
}
int ADQ_SetClockSource(void* adq_cu_ptr, int adq_no, int) {
	return 1;
}
